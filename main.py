import datetime
import time
import logging
from logging.handlers import RotatingFileHandler

from sqlalchemy.orm import Session

from app.database.models import Tax, FilterTax
from app.database.database import SessionLocal

# noinspection PyArgumentList
logging.basicConfig(
    handlers=[
        RotatingFileHandler("logs/log.log", maxBytes=100000000, backupCount=50)
    ],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)


def start(db: Session = SessionLocal()):
    try:
        db.query(FilterTax).delete()
        logging.info("All filter_taxes deleted")
        tic = time.perf_counter()
        logging.info(f"Started new bunch of filter_taxes")
        total_count = 0
        count = 0
        res = []
        for c_id in db.query(Tax.company_id).distinct():
            if count == 1000:
                db.add_all(res)
                logging.info(f"Added new bunch of filter_taxes to db, count: {count}, total_count: {total_count}")
                db.commit()
                logging.info(f"### DATABASE QUERY SUCCESSFUL")
                count = 0
                res = []

            taxes = db.query(Tax).filter_by(company_id=c_id[0]).all()
            if not taxes or c_id[0] is None:
                logging.info(f"No taxes records found for company_id {c_id}")
                print("No records")
                continue

            filter_tax = FilterTax()
            filter_tax.company_id = c_id[0]
            for tax in taxes:
                if tax.year == 2015:
                    logging.info(f"Company_id: {c_id[0]}, tax_2015: {tax.amount}, filter_tax.y_2015: {filter_tax.y_2015}")
                    filter_tax.y_2015 = tax.amount
                    filter_tax.y_2016 = (filter_tax.y_2016 if filter_tax.y_2016 else 0) + tax.amount
                    filter_tax.y_2017 = (filter_tax.y_2017 if filter_tax.y_2017 else 0) + tax.amount
                    filter_tax.y_2018 = (filter_tax.y_2018 if filter_tax.y_2018 else 0) + tax.amount
                    filter_tax.y_2019 = (filter_tax.y_2019 if filter_tax.y_2019 else 0) + tax.amount
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2016:
                    logging.info(f"Company_id: {c_id[0]}, tax_2016: {tax.amount}, filter_tax.y_2016: {filter_tax.y_2016}")
                    filter_tax.y_2016 = (filter_tax.y_2016 if filter_tax.y_2016 else 0) + tax.amount
                    filter_tax.y_2017 = (filter_tax.y_2017 if filter_tax.y_2017 else 0) + tax.amount
                    filter_tax.y_2018 = (filter_tax.y_2018 if filter_tax.y_2018 else 0) + tax.amount
                    filter_tax.y_2019 = (filter_tax.y_2019 if filter_tax.y_2019 else 0) + tax.amount
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2017:
                    logging.info(f"Company_id: {c_id[0]}, tax_2017: {tax.amount}, filter_tax.y_2017: {filter_tax.y_2017}")
                    filter_tax.y_2017 = (filter_tax.y_2017 if filter_tax.y_2017 else 0) + tax.amount
                    filter_tax.y_2018 = (filter_tax.y_2018 if filter_tax.y_2018 else 0) + tax.amount
                    filter_tax.y_2019 = (filter_tax.y_2019 if filter_tax.y_2019 else 0) + tax.amount
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2018:
                    logging.info(f"Company_id: {c_id[0]}, tax_2018: {tax.amount}, filter_tax.y_2018: {filter_tax.y_2018}")
                    filter_tax.y_2018 = (filter_tax.y_2018 if filter_tax.y_2018 else 0) + tax.amount
                    filter_tax.y_2019 = (filter_tax.y_2019 if filter_tax.y_2019 else 0) + tax.amount
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2019:
                    logging.info(f"Company_id: {c_id[0]}, tax_2019: {tax.amount}, filter_tax.y_2019: {filter_tax.y_2019}")
                    filter_tax.y_2019 = (filter_tax.y_2019 if filter_tax.y_2019 else 0) + tax.amount
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2020:
                    logging.info(f"Company_id: {c_id[0]}, tax_2020: {tax.amount}, filter_tax.y_2020: {filter_tax.y_2020}")
                    filter_tax.y_2020 = (filter_tax.y_2020 if filter_tax.y_2020 else 0) + tax.amount
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2021:
                    logging.info(f"Company_id: {c_id[0]}, tax_2021: {tax.amount}, filter_tax.y_2021: {filter_tax.y_2021}")
                    filter_tax.y_2021 = (filter_tax.y_2021 if filter_tax.y_2021 else 0) + tax.amount
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount
                if tax.year == 2022:
                    logging.info(f"Company_id: {c_id[0]}, tax_2022: {tax.amount}, filter_tax.y_2022: {filter_tax.y_2021}")
                    filter_tax.y_2022 = (filter_tax.y_2022 if filter_tax.y_2022 else 0) + tax.amount

            # filter_tax.created = datetime.datetime.now()
            res.append(filter_tax)
            count += 1
            total_count += 1
        db.add_all(res)
        logging.info(f"Added new bunch of filter_taxes to db, count: {count}, total_count: {total_count}")
        db.commit()
        logging.info(f"### DATABASE QUERY SUCCESSFUL")
        toc = time.perf_counter()
        logging.info(f"Done the migration in {toc - tic:0.4f} seconds")

        return True
    except Exception as e:
        logging.exception(e)
        return False
    finally:
        db.close()


def init_db():
    from app.database.database import Base, engine
    Base.metadata.create_all(bind=engine)


if __name__ == "__main__":
    # init_db()
    start()
