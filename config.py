import os

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())


class Settings:
    PROJECT_NAME: str = "Kompra Filter"
    PROJECT_VERSION: str = "1.0.0"
    ROOT_PATH: str = os.getenv("ROOT_PATH", "")

    APP_ENV: str = os.getenv("APP_ENV", "production")
    APP_DEBUG: str = os.getenv("APP_DEBUG", "false")
    APP_LOG_LEVEL: str = os.getenv("APP_LOG_LEVEL", "info")

    POSTGRES_USER: str = os.getenv("POSTGRES_USER")
    POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER", "localhost")
    POSTGRES_PORT: str = os.getenv("POSTGRES_PORT", 5432)
    POSTGRES_DB: str = os.getenv("POSTGRES_DB", "tdd")
    DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"


settings = Settings()

if not os.path.exists("logs"):
    os.makedirs("logs")
