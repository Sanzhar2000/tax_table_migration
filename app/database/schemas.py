from pydantic import BaseModel


class OkedBase(BaseModel):
    name: str
    parent_oked: str


class Oked(OkedBase):
    oked: str
