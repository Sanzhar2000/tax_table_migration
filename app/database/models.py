import datetime

from sqlalchemy import (JSON, TIMESTAMP, Boolean, Column, DateTime, Float,
                        ForeignKey, Integer, String, Text)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from app.database.database import Base


class Company(Base):
    __tablename__ = "company"

    id = Column(Integer, primary_key=True, index=True)

    iin = Column(String)
    full_name = Column(String)
    name = Column(String)

    created = Column(DateTime(timezone=True), server_default=func.now())
    last_updated = Column(DateTime(timezone=True), onupdate=func.now())

    bin = Column(String, unique=True)
    fact_address = Column(Text)
    field = Column(Text)
    law_address = Column(Text)
    ip = Column(Boolean)

    oked = Column(String, ForeignKey("oked.oked"))
    okpo = Column(String)
    kato = Column(String)
    workers = Column(String)
    region = Column(String)
    register_date = Column(DateTime(timezone=True))
    active = Column(Boolean)
    unreliable = Column(Boolean)
    bad_supplier = Column(Boolean)
    size = Column(String)
    ownership = Column(String)

    tax = relationship("Tax", back_populates="company")
    filter_taxes = relationship("FilterTax", back_populates="company")


class Tax(Base):
    __tablename__ = "tax"

    id = Column(Integer, primary_key=True, index=True)

    amount = Column(Float)
    year = Column(Integer)
    company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)
    nds = Column(Float)
    bin = Column(Text)

    company = relationship("Company", back_populates="tax")


class FilterTax(Base):
    __tablename__ = "filter_taxes"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    identifier = Column(String)
    company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)

    y_2015 = Column(Float, default=0)
    y_2016 = Column(Float, default=0)
    y_2017 = Column(Float, default=0)
    y_2018 = Column(Float, default=0)
    y_2019 = Column(Float, default=0)
    y_2020 = Column(Float, default=0)
    y_2021 = Column(Float, default=0)
    y_2022 = Column(Float, default=0)
    y_2023 = Column(Float, default=0)
    y_2024 = Column(Float, default=0)
    y_2025 = Column(Float, default=0)

    created = Column(TIMESTAMP, default=func.now())
    last_updated = Column(TIMESTAMP, default=func.now(), onupdate=func.now())

    company = relationship("Company", back_populates="filter_taxes")
